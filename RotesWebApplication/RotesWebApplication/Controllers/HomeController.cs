﻿using RotesWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RotesWebApplication.Controllers
{
    public class HomeController : Controller
    {


        private readonly IEnumerable<Noticia> noticias;

        public HomeController()
        {
            noticias = new Noticia().TodasNoticias().OrderByDescending(x => x.Date);

        }

        public ActionResult Index()
        {
            //Take() Busca os dois ultimos registros
            var ultimasNoticias = noticias.Take(2);

            //var todasCategorias = noticias.Select(x => x.Categoria).Distinct();

            //ViewBag.Categorias = todasCategorias;

            ViewBag.Categorias = noticias.Select(x => x.Categoria).Distinct();

            ViewBag.ultimasNoticias = ultimasNoticias;

            ViewBag.TituloIndex = "Lista de Notícias";

            ViewBag.Conteudos = noticias.Select(x => x.Conteudo).Distinct();


            return View(ultimasNoticias);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}