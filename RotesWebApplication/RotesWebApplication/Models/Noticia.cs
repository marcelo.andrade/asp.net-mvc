﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RotesWebApplication.Models
{
    public class Noticia
    {
        public int NoticiaId { get; set; }
        public string Titulo { get; set; }
        public string Conteudo { get; set; }
        public string Categoria { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        public IEnumerable<Noticia> TodasNoticias()
        {
            var retorno = new Collection<Noticia>
            {
                new Noticia
                {
                    NoticiaId = 1,
                    Titulo = "Felipe Massa ganha F1",
                    Conteudo = "Primeira vitória de Felipe",
                    Categoria = "Esportes",
                    Date = new DateTime(2004,9,29)
                },
                new Noticia
                {
                    NoticiaId = 2,
                    Titulo = "Vacina H1N1",
                    Categoria = "Saúde",
                    Conteudo = "Encontrado fator x do vírus",
                    Date = new DateTime(2009,9,29)
                },
                new Noticia
                {
                    NoticiaId = 3,
                    Titulo = "Prova Enem 2019",
                    Categoria = "Educação",
                    Conteudo = "Prova adiada, motivo: fralde",
                    Date = new DateTime(2015,9,29)
                },
                
                new Noticia
                {
                    NoticiaId = 4,
                    Titulo = "Cultura Negra",
                    Categoria = "História",
                    Conteudo = "Apresentações e Jograis",
                    Date = new DateTime(2020,9,29)
                }
            };

            return retorno;
        }
        
         
    }
}