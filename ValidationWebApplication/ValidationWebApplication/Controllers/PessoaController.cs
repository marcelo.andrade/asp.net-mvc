﻿using System.Web.Mvc;
using ValidationWebApplication.Models;
using System.Collections.ObjectModel;
using System.Linq;

namespace ValidationWebApplication.Controllers
{
    public class PessoaController : Controller
    {
        // GET: Pessoa
        public ActionResult Index()
        {
            var pessoa = new Pessoa();
            return View(pessoa);
        }

        //POST: Pessoa
        [HttpPost]
        public ActionResult Index(Pessoa pessoa)
        {
            if (pessoa.Pass != pessoa.PassConfirm)
            {
                ModelState.AddModelError("", "The pass do not match!");
            }

            //Validation in server
            if (ModelState.IsValid)
            {
                return View("Result", pessoa);
            }

            return View(pessoa);
        }

        public ActionResult Result(Pessoa pessoa)
        {
            return View(pessoa);
        }

        public ActionResult ValidateLogin(string login)
        {
            var collection = new Collection<string>
            {"Marcelo", "Joao", "Maria" };

            return Json(collection.All(x => x.ToLower() != login.ToLower()), JsonRequestBehavior.AllowGet);
        }
    }
}