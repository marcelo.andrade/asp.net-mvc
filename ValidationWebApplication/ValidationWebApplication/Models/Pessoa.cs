﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CompareAttribute = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace ValidationWebApplication.Models
{
    public class Pessoa
    {
        [Required(ErrorMessage = "The field Name is required")]
        public string Name { get; set; }

        //[Required]
        [Range(18, 50)]
        public int Age { get; set; }

        [Required]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "E-mail em formato inválido.")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"[a-zA-Z]{5,15}", ErrorMessage = "O Login deve conter no entre 5 e 15 caracteres, somente letras.")]
        //Remote validation
        [Remote("ValidateLogin", "Pessoa", ErrorMessage = "Esse login já está em uso")]
        public string Login { get; set; }

        [Required]
        public string Pass { get; set; }

        [Compare("Pass")]
        public string PassConfirm { get; set; }
    }
}